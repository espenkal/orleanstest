# Orleans test #

Each solution folder should probably be a separate git repo and the OrleansTest.Common should be shared with the clients as a nuget package 


**Resources**

- Orleans
	- https://dotnet.github.io/orleans/
- Orleans security
	- https://github.com/Async-Hub/Orleans.Security
	- https://mcguirev10.com/2019/12/19/securing-orleans-microservices-with-openid-connect.html
- Orleans event sourcing
	- https://mcguirev10.com/2019/12/05/event-sourcing-with-orleans-journaled-grains.html
- Multi tenant database
	- https://www.carlrippon.com/creating-an-aspnetcore-multi-tenant-web-api-with-dapper-and-sql-rls/


**To run**

1. Create a LocalDB database with the name OrleansTest
1. Run all the SQL scripts on the new database
1. Run at least one instance of the Silo porject
1. Run one instance of the Client project
1. Go to https://localhost:8077/?tenantId=6CB8DE43-2043-4415-B267-7FFFA2EB5AC0&personId=775EDB92-32BE-4D46-ABBB-921BC1860514 to trigger some data request
1. Go to http://localhost:8090/ to see what is happening in Orleans