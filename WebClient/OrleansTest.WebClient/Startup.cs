﻿using Microsoft.AspNetCore.HttpOverrides;
using Orleans;
using Orleans.Hosting;
using Orleans.Runtime;

namespace OrleansTest.Client
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.XForwardedProto | ForwardedHeaders.XForwardedHost;
            });
            services.AddHttpContextAccessor();

            // Set up the Orleans client on the service collection 
            services.AddSingleton<OrleansClientService>();
            services.AddSingleton<IHostedService>(sp => sp.GetRequiredService<OrleansClientService>());
            services.AddSingleton<IClusterClient>(sp => sp.GetRequiredService<OrleansClientService>().Client);
            services.AddSingleton<IGrainFactory>(sp => sp.GetRequiredService<OrleansClientService>().Client);
            services.AddControllers();
            services.AddSwaggerGen();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseForwardedHeaders();
            app.UseHttpsRedirection();

            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthorization();
            app.UseSwagger();
            app.UseSwaggerUI();

            app.Use((a, next) =>
            {
                RequestContext.Set("UserId", Guid.Parse("6CB8DE43-2043-4415-B267-7FFFA2EB5AC0"));
                RequestContext.Set("TenantId", Guid.Parse("775EDB92-32BE-4D46-ABBB-921BC1860514"));
                return next();
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}