﻿using Orleans;
using Orleans.Hosting;

namespace OrleansTest.Client
{
    // dotnet run --project .\WebClient\OrleansTest.WebClient\
    public class Program
    {
        public static void Main(string[] args)
        {
            var hostBuilder = Host.CreateDefaultBuilder(args);
            hostBuilder.ConfigureWebHostDefaults(webHostBuilder =>
            {
                webHostBuilder.UseStartup<Startup>();
            });
            hostBuilder.ConfigureLogging(loggingBuilder =>
            {
                loggingBuilder.AddConsole();
            });

            var host = hostBuilder.Build();
            host.Run();
        }
    }
}