﻿using Microsoft.AspNetCore.Mvc;
using Orleans;
using OrleansTest.Common.Grains;

namespace OrleansTest.Client.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AbsenceApplicationController : ControllerBase
    {
        private readonly IClusterClient clusterClient;

        public AbsenceApplicationController(IClusterClient clusterClient)
        {
            this.clusterClient = clusterClient;
        }

        [HttpGet]
        public async Task<IActionResult> Get(Guid id)
        {
            var absenceApplicationGrain = clusterClient.GetGrain<IAbsenceApplicationGrain>(id);
            var absenceApplication = await absenceApplicationGrain.Get();
            return Ok(absenceApplication);
        }

        [HttpPost]
        public async Task<IActionResult> Create(string comment)
        {
            var absenceApplicationGrain = clusterClient.GetGrain<IAbsenceApplicationGrain>(Guid.NewGuid());
            await absenceApplicationGrain.Create(comment);
            return Ok();
        }

        [HttpPatch("approve")]
        public async Task<IActionResult> Approve(Guid id, string comment)
        {
            var absenceApplicationGrain = clusterClient.GetGrain<IAbsenceApplicationGrain>(id);
            await absenceApplicationGrain.Approve(comment);
            return Ok();
        }

        [HttpPatch("decline")]
        public async Task<IActionResult> Decline(Guid id, string comment)
        {
            var absenceApplicationGrain = clusterClient.GetGrain<IAbsenceApplicationGrain>(id);
            await absenceApplicationGrain.Decline(comment);
            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            var absenceApplicationGrain = clusterClient.GetGrain<IAbsenceApplicationGrain>(id);
            await absenceApplicationGrain.Delete();
            return Ok();
        }
    }
}