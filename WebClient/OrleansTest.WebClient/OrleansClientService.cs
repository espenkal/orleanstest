﻿using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using Orleans.Runtime;

namespace OrleansTest.Client
{
    public class OrleansClientService : IHostedService
    {
        private Guid userId = Guid.Parse("877B5E5E-6D07-4723-9427-2EAD7132D603");
        private Guid traceId = Guid.Parse("84DAFE77-1CE9-4439-9582-919649690030");
        public IClusterClient Client { get; }

        public OrleansClientService(ILoggerProvider loggerProvider)
        {
            try
            {
                RequestContext.Set("TraceId", traceId);

                var invariant = "System.Data.SqlClient";
                var connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=OrleansTest;Integrated Security=True;Pooling=False;Max Pool Size=200;MultipleActiveResultSets=True";

                var clientBuilder = new ClientBuilder();

                // Cluster setup
                clientBuilder.Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = "OrleansTest";
                    options.ServiceId = "TestApp";
                });
                clientBuilder.UseAdoNetClustering(options =>
                {
                    options.Invariant = invariant;
                    options.ConnectionString = connectionString;
                });

                // Configure what grains we want to use
                clientBuilder.ConfigureApplicationParts(parts => parts.AddFromApplicationBaseDirectory());

                clientBuilder.ConfigureLogging(builder => builder.AddProvider(loggerProvider));
                Client = clientBuilder.Build();

            }
            catch (Exception e)
            {
                Console.WriteLine($"\nException while trying to run client: {e.Message}");
                Console.WriteLine("Make sure the silo the client is trying to connect to is running.");
                Console.WriteLine("\nPress any key to exit.");
            }
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            // A retry filter could be provided here.
            await Client.Connect();
            Console.WriteLine("Client successfully connected to silo host(s) \n");

            //// Connect to the UserTask stream for the user
            //var streamProvider = Client.GetStreamProvider("DomainEvents");
            //var userTaskStream = streamProvider.GetStream<UserTask>(userId, "Person.UserTask");
            //await userTaskStream.SubscribeAsync(new UserTaskObserver());
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await Client.Close();

            Client.Dispose();
        }
    }
}