﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using Orleans.Statistics;
using OrleansTest.DataAccess.Interfaces;

namespace OrleansTest.Silo
{
    public class Program
    {
        // dotnet run --project .\Domain\OrleansTest.Silo\
        public static async Task Main(string[] args)
        {
            try
            {
                var invariant = "System.Data.SqlClient"; // for Microsoft SQL Server
                var connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=OrleansTest;Integrated Security=True;Pooling=False;Max Pool Size=200;MultipleActiveResultSets=True";

                var siloHostBuilder = new SiloHostBuilder();

                // Cluster setup
                siloHostBuilder.Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = "OrleansTest";
                    options.ServiceId = "TestApp";
                });
                siloHostBuilder.ConfigureEndpoints(siloPort: 11111, gatewayPort: 30000);
                siloHostBuilder.UseAdoNetClustering(options =>
                {
                    options.Invariant = invariant;
                    options.ConnectionString = connectionString;
                }); // See "node" info data in OrleansMembershipTable SQL table

                // Stream
                siloHostBuilder.AddSimpleMessageStreamProvider("stream-provider");
                siloHostBuilder.AddAdoNetGrainStorage("PubSubStore", options => // PubSubStore is required for SimpleMessageStreamProvider
                {
                    options.Invariant = invariant;
                    options.ConnectionString = connectionString;
                });  // See data in OrleansStorage SQL table

                // Grain storage
                siloHostBuilder.AddAdoNetGrainStorage("GrainStorage", options =>
                {
                    options.Invariant = invariant;
                    options.ConnectionString = connectionString;
                }); // See data in OrleansStorage SQL table

                // Monitoring
                siloHostBuilder.UsePerfCounterEnvironmentStatistics();
                siloHostBuilder.UseDashboard(options =>
                {
                    options.Port = 8090;
                }); // Se overview/performance of the silos and grains on http://localhost:8090
                siloHostBuilder.ConfigureLogging(logging => logging.AddConsole());

                // Other dependencies
                siloHostBuilder.ConfigureServices(services =>
                {
                    services.AddSingleton<ISqlConnectionProvider, SqlConnectionProvider>();
                    services.AddSingleton<IAbsenceApplicationDataAccess, DataAccess.AbsenceApplicationDataAccess>();
                    services.AddSingleton<IAbsenceApplicationApproverDataAccess, DataAccess.AbsenceApplicationApproverDataAccess>();
                    services.AddSingleton<IAbsenceApplicationEventDataAccess, DataAccess.AbsenceApplicationEventDataAccess>();
                });

                // Configure what grains we want to "host"
                siloHostBuilder.ConfigureApplicationParts(parts => parts.AddFromApplicationBaseDirectory());


                var host = siloHostBuilder.Build();
                await host.StartAsync();

                Console.WriteLine("\n Dashboard: http://localhost:8090\n");
                Console.WriteLine("\n Press Enter to terminate...\n");
                Console.ReadLine();
                await host.StopAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}