﻿using Dapper;
using Microsoft.Data.SqlClient;
using Orleans.Runtime;
using OrleansTest.DataAccess.Interfaces;

namespace OrleansTest.Silo
{
    public class SqlConnectionProvider : ISqlConnectionProvider
    {
        public async Task<SqlConnection> GetTenantSqlConnection()
        {
            try
            {
                var connection = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=OrleansTest;Integrated Security=True;Pooling=False;Max Pool Size=200;MultipleActiveResultSets=True");
                await connection.OpenAsync();
                await connection.ExecuteAsync("EXEC dbo.sp_set_session_context @key = N'TenantId', @value = @TenantId", new
                {
                    TenantId = (Guid)RequestContext.Get("TenantId")
                });
                return connection;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}