using Microsoft.Extensions.DependencyInjection;
using Orleans.Hosting;
using Orleans.TestingHost;
using OrleansTest.DataAccess.Interfaces;

namespace OrleansTest.Silo.Test
{
    public class TestSiloConfigurations : ISiloConfigurator
    {
        public void Configure(ISiloBuilder siloBuilder)
        {
            siloBuilder.ConfigureServices(services =>
            {
                services.AddSingleton<ISqlConnectionProvider, SqlConnectionProvider>();
            });
        }
    }
}