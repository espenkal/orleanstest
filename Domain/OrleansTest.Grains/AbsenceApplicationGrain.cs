﻿using Orleans;
using Orleans.Providers;
using Orleans.Runtime;
using Orleans.Streams;
using OrleansTest.Common.Grains;
using OrleansTest.DataAccess.Interfaces;
using OrleansTest.Grains.Events;

namespace OrleansTest.Grains
{
    [StorageProvider(ProviderName = "GrainStorage")]
    public class AbsenceApplicationGrain : Grain<Common.Models.AbsenceApplication>, IAbsenceApplicationGrain
    {
        private readonly IAbsenceApplicationDataAccess absenceApplicationDataAccess;
        private readonly IAbsenceApplicationEventDataAccess absenceApplicationEventDataAccess;
        private readonly IAbsenceApplicationApproverDataAccess absenceApplicationApproverDataAccess;
        private readonly IGrainFactory grainFactory;
        private IAsyncStream<AbsenceApplicationEventBase>? stream;

        private Guid Id { get => this.GetPrimaryKey(); }
        private Guid UserId { get => (Guid)RequestContext.Get("UserId"); }

        public AbsenceApplicationGrain(
            IAbsenceApplicationDataAccess absenceApplicationDataAccess,
            IAbsenceApplicationEventDataAccess absenceApplicationEventDataAccess,
            IAbsenceApplicationApproverDataAccess absenceApplicationApproverDataAccess,
            IGrainFactory grainFactory)
        {
            this.absenceApplicationDataAccess = absenceApplicationDataAccess;
            this.absenceApplicationEventDataAccess = absenceApplicationEventDataAccess;
            this.absenceApplicationApproverDataAccess = absenceApplicationApproverDataAccess;
            this.grainFactory = grainFactory;
        }

        public override async Task OnActivateAsync()
        {
            State.Id = Id;
            var existing = await absenceApplicationDataAccess.Get(State.Id);
            if(existing != null)
            {
                State = existing;
            }
            else
            {
                State.Status = Common.Enums.AbsenceApplicationStatus.Undefined;
            }

            var streamProvider = GetStreamProvider("stream-provider");
            stream = streamProvider.GetStream<AbsenceApplicationEventBase>(Id, "AbsenceApplication");
            await stream.SubscribeAsync(OnAbsenceApplicationEvent);

            await base.OnActivateAsync();
        }

        public Task OnAbsenceApplicationEvent(AbsenceApplicationEventBase eventData, StreamSequenceToken? token = null)
        {
            Console.WriteLine($"\n\n==============> Event: {eventData.GetType()}\n\n");

            absenceApplicationEventDataAccess.Create(Guid.NewGuid(), eventData.EventTypeId, State.Id, State.Status, UserId);

            // Publish to rabbit etc
            return Task.CompletedTask;
        }






        public async Task Create(string comment)
        {
            await absenceApplicationDataAccess.Create(Id);
            await stream.OnNextAsync(new AbsenceApplicationCreatedEvent());

            // Add period

            // Add approvers
            var absenceApplicationApproverGrain = grainFactory.GetGrain<IAbsenceApplicationApproverGrain>($"{Id}_{UserId}");
            await absenceApplicationApproverGrain.Create();

            //Change status to "open" the entity for use
            State.Status = Common.Enums.AbsenceApplicationStatus.Awaiting;
            await stream.OnNextAsync(new AbsenceApplicationProposalCreatedEvent());

        }

        public async Task Approve(string comment)
        {
            // Set status for user
            var absenceApplicationApproverGrain = grainFactory.GetGrain<IAbsenceApplicationApproverGrain>($"{Id}_{UserId}");
            await absenceApplicationApproverGrain.Approve();

            // Set application status
            if (await absenceApplicationApproverDataAccess.IsAllApproved())
            {
                State.Status = Common.Enums.AbsenceApplicationStatus.Approved;
                await stream.OnNextAsync(new AbsenceApplicationApprovedEvent());
            }

            // Add comment
        }

        public async Task Decline(string comment)
        {
            // Set status for user
            var absenceApplicationApproverGrain = grainFactory.GetGrain<IAbsenceApplicationApproverGrain>($"{Id}_{UserId}");
            await absenceApplicationApproverGrain.Decline();

            // Set application status
            if (await absenceApplicationApproverDataAccess.HasUndecided())
            {
                State.Status = Common.Enums.AbsenceApplicationStatus.Awaiting;
            }
            else if (await absenceApplicationApproverDataAccess.IsApproved())
            {
                State.Status = Common.Enums.AbsenceApplicationStatus.Conflicted;
                await stream.OnNextAsync(new AbsenceApplicationConflictedEvent());
            }
            else
            {
                State.Status = Common.Enums.AbsenceApplicationStatus.Declined;
                await stream.OnNextAsync(new AbsenceApplicationDeclinedEvent());
            }

            // Add comment
        }

        public async Task Delete()
        {
            await absenceApplicationDataAccess.Delete(Id);
            // TODO
        }

        public Task<Common.Models.AbsenceApplication> Get()
        {
            return Task.FromResult(State);
        }
    }
}