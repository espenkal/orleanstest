﻿using Orleans;
using Orleans.Providers;
using Orleans.Streams;
using OrleansTest.Common.Enums;
using OrleansTest.Common.Grains;
using OrleansTest.Common.Models;
using OrleansTest.DataAccess.Interfaces;
using OrleansTest.Grains.Events;

namespace OrleansTest.Grains
{
    [StorageProvider(ProviderName = "GrainStorage")]
    public class AbsenceApplicationApproverGrain : Grain<AbsenceApplicationApprover>, IAbsenceApplicationApproverGrain
    {
        private readonly IAbsenceApplicationApproverDataAccess absenceApplicationApproverDataAccess;
        private IAsyncStream<AbsenceApplicationEventBase>? stream;

        public AbsenceApplicationApproverGrain(IAbsenceApplicationApproverDataAccess absenceApplicationApproverDataAccess)
        {
            this.absenceApplicationApproverDataAccess = absenceApplicationApproverDataAccess;
        }

        public override async Task OnActivateAsync()
        {
            var ids = this.GetPrimaryKeyString().Split("_");
            State.AbsenceApplicationId = Guid.Parse(ids[0]);
            State.ApproverId = Guid.Parse(ids[1]);

            var existing = await absenceApplicationApproverDataAccess.Get(State.AbsenceApplicationId, State.ApproverId);
            if (existing != null)
            {
                State = existing;
            }
            else
            {
                State.Id = Guid.NewGuid();
                State.Status = Common.Enums.AbsenceApplicationApproverStatus.Undefined;
            }

            var streamProvider = GetStreamProvider("stream-provider");
            stream = streamProvider.GetStream<AbsenceApplicationEventBase>(State.AbsenceApplicationId, "AbsenceApplication");

            await base.OnActivateAsync();
        }

        public async Task Approve()
        {
            State.Status = Common.Enums.AbsenceApplicationApproverStatus.Approved;
            await absenceApplicationApproverDataAccess.SetStatus(State.Id, State.Status);
            await stream.OnNextAsync(new AbsenceApplicationApproverApprovedEvent(State.ApproverId));
        }

        public async Task Decline()
        {
            State.Status = Common.Enums.AbsenceApplicationApproverStatus.Declined;
            await absenceApplicationApproverDataAccess.SetStatus(State.Id, State.Status);
            await stream.OnNextAsync(new AbsenceApplicationApproverDeclinedEvent(State.ApproverId));
        }

        public async Task Delete()
        {
            State.Status = Common.Enums.AbsenceApplicationApproverStatus.Deleted;
            await absenceApplicationApproverDataAccess.Delete(State.Id);
            await stream.OnNextAsync(new AbsenceApplicationApproverRemovedEvent(State.ApproverId));
        }

        public async Task Create()
        {
            State.Status = AbsenceApplicationApproverStatus.Awaiting;
            await absenceApplicationApproverDataAccess.Create(State.Id, State.AbsenceApplicationId, State.ApproverId, State.Status);
            await stream.OnNextAsync(new AbsenceApplicationApproverAddedEvent(State.ApproverId));
        }
    }
}