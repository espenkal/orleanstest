﻿namespace OrleansTest.Grains.Events
{
    public abstract record EventBase(Guid EventTypeId) { }
}