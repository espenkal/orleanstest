﻿namespace OrleansTest.Grains.Events
{
    public abstract record AbsenceApplicationEventBase(Guid EventTypeId) : EventBase(EventTypeId) { };
    public record AbsenceApplicationProposalCreatedEvent() : AbsenceApplicationEventBase(Guid.Parse("B5EFE83E-4D91-45B5-9B58-C4CACFE6BED5")) { }
    public record AbsenceApplicationCreatedEvent() : AbsenceApplicationEventBase(Guid.Parse("E42BDDFA-EA00-4544-99C2-CF4224D7401E")) { }
    public record AbsenceApplicationApprovedEvent() : AbsenceApplicationEventBase(Guid.Parse("569954A8-9E46-40F0-B473-6D56852628EF")) { }
    public record AbsenceApplicationDeclinedEvent() : AbsenceApplicationEventBase(Guid.Parse("F96F8DCA-47B6-4BEC-BFF4-4AE02DFE2298")) { }
    public record AbsenceApplicationConflictedEvent() : AbsenceApplicationEventBase(Guid.Parse("45DE090E-8E67-40E3-B279-43E3D1FC2A63")) { }
    public record AbsenceApplicationCommentAddedEvent(string Comment) : AbsenceApplicationEventBase(Guid.Parse("EC584097-0B77-4E01-B7A6-3D3B807062A7")) { }

    public abstract record AbsenceApplicationApproverEventBase(Guid EventTypeId, Guid ApproverId) : AbsenceApplicationEventBase(EventTypeId) { }
    public record AbsenceApplicationApproverAddedEvent(Guid ApproverId) : AbsenceApplicationApproverEventBase(Guid.Parse("573A1EF1-3652-4B79-81AF-34B49B5AAECA"), ApproverId) { }
    public record AbsenceApplicationApproverApprovedEvent(Guid ApproverId) : AbsenceApplicationApproverEventBase(Guid.Parse("A5570884-B3DC-46B0-81AF-DBEF195657C2"), ApproverId) { }
    public record AbsenceApplicationApproverDeclinedEvent(Guid ApproverId) : AbsenceApplicationApproverEventBase(Guid.Parse("F9467D61-E312-49A6-AED2-FA9B51A419CA"), ApproverId) { }
    public record AbsenceApplicationApproverRemovedEvent(Guid ApproverId) : AbsenceApplicationApproverEventBase(Guid.Parse("CFA2475A-44ED-49D0-8147-086581C7732F"), ApproverId) { }
}