﻿using OrleansTest.Common.Enums;

namespace OrleansTest.Common.Models
{
    public class AbsenceApplicationApprover: ModelBase
    {
        public Guid ApproverId { get; set; }
        public Guid AbsenceApplicationId { get; set; }
        public AbsenceApplicationApproverStatus Status { get; set; }
    }
}