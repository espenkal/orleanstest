﻿namespace OrleansTest.Common.Models
{
    public class AbsenceApplication : ModelBase
    {
        public Enums.AbsenceApplicationStatus Status { get; set; }
    }
}