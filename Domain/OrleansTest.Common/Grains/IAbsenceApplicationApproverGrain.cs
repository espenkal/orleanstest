﻿namespace OrleansTest.Common.Grains
{
    public interface IAbsenceApplicationApproverGrain : Orleans.IGrainWithStringKey
    {
        Task Approve();
        Task Decline();
        Task Delete();
        Task Create();
    }
}