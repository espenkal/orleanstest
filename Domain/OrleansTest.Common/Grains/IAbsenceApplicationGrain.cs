﻿using OrleansTest.Common.Models;

namespace OrleansTest.Common.Grains
{
    public interface IAbsenceApplicationGrain : Orleans.IGrainWithGuidKey
    {
        Task<AbsenceApplication> Get();
        Task Create(string comment);
        Task Delete();
        Task Decline(string comment);
        Task Approve(string comment);
    }
}