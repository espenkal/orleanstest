﻿namespace OrleansTest.Common.Enums
{
    public enum AbsenceApplicationStatus
    {
        Awaiting,
        Declined,
        Approved,
        Deleted,
        Conflicted,
        Undefined
    }
    public enum AbsenceApplicationApproverStatus
    {
        Declined,
        Approved,
        Deleted,
        Awaiting,
        Undefined
    }
}
