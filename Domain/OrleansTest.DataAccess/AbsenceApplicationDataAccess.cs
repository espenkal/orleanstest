﻿using OrleansTest.Common.Models;
using OrleansTest.DataAccess.Interfaces;

namespace OrleansTest.DataAccess
{
    public class AbsenceApplicationDataAccess : IAbsenceApplicationDataAccess
    {
        public Task Create(Guid id)
        {
            return Task.CompletedTask;
        }

        public Task Delete(Guid id)
        {
            return Task.CompletedTask;
        }

        public Task<AbsenceApplication> Get(Guid id)
        {
            return Task.FromResult(new AbsenceApplication()
            {
                Id = id,
                Status = Common.Enums.AbsenceApplicationStatus.Conflicted
            });
        }
    }
}