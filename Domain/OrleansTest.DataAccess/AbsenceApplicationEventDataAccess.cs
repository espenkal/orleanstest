﻿using OrleansTest.Common.Enums;
using OrleansTest.DataAccess.Interfaces;

namespace OrleansTest.DataAccess
{
    public class AbsenceApplicationEventDataAccess : IAbsenceApplicationEventDataAccess
    {
        public Task Create(Guid id, Guid eventTypeId, Guid absenceApplicationId, AbsenceApplicationStatus absenceApplicationStatus, Guid triggeredById)
        {
            return Task.CompletedTask;
        }
    }
}