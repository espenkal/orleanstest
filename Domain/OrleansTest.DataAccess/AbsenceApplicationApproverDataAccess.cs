﻿using OrleansTest.Common.Enums;
using OrleansTest.Common.Models;
using OrleansTest.DataAccess.Interfaces;

namespace OrleansTest.DataAccess
{
    public class AbsenceApplicationApproverDataAccess : IAbsenceApplicationApproverDataAccess
    {
        public Task Create(Guid id, Guid absenceApplicationId, Guid approverId, AbsenceApplicationApproverStatus awaiting)
        {
            return Task.CompletedTask;
        }

        public Task Delete(Guid id)
        {
            return Task.CompletedTask;
        }

        public Task<AbsenceApplicationApprover?> Get(Guid guid, Guid approverId)
        {
            return Task.FromResult<AbsenceApplicationApprover?>(null);
        }

        public Task<bool> HasUndecided()
        {
            return Task.FromResult(true);
        }

        public Task<bool> IsAllApproved()
        {
            return Task.FromResult(true);
        }

        public Task<bool> IsApproved()
        {
            return Task.FromResult(true);
        }

        public Task SetStatus(Guid id, AbsenceApplicationApproverStatus status)
        {
            return Task.CompletedTask;
        }
    }
}