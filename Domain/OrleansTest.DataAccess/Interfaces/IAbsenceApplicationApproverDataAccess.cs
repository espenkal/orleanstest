﻿using OrleansTest.Common.Enums;
using OrleansTest.Common.Models;

namespace OrleansTest.DataAccess.Interfaces
{
    public interface IAbsenceApplicationApproverDataAccess
    {
        Task Create(Guid id, Guid absenceApplicationId, Guid approverId, AbsenceApplicationApproverStatus awaiting);
        Task SetStatus(Guid id, AbsenceApplicationApproverStatus status);
        Task Delete(Guid id);
        Task<AbsenceApplicationApprover?> Get(Guid guid, Guid approverId);
        Task<bool> IsAllApproved();
        Task<bool> IsApproved();
        Task<bool> HasUndecided();
    }
}