﻿using OrleansTest.Common.Enums;

namespace OrleansTest.DataAccess.Interfaces
{
    public interface IAbsenceApplicationEventDataAccess
    {
        Task Create(Guid id, Guid eventTypeId, Guid absenceApplicationId, AbsenceApplicationStatus absenceApplicationStatus, Guid triggeredById);
    }
}