﻿using Microsoft.Data.SqlClient;

namespace OrleansTest.DataAccess.Interfaces
{
    public interface ISqlConnectionProvider
    {
        Task<SqlConnection> GetTenantSqlConnection();
    }
}