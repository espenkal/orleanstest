﻿namespace OrleansTest.DataAccess.Interfaces
{
    public interface IAbsenceApplicationDataAccess
    {
        Task<Common.Models.AbsenceApplication> Get(Guid id);
        Task Create(Guid id);
        Task Delete(Guid id);
    }
}