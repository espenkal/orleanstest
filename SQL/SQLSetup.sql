﻿CREATE DATABASE OrleansTest
GO

USE OrleansTest
GO

CREATE LOGIN applicationuserlogin WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER applicationuser FOR LOGIN applicationuserlogin
GO
EXEC sp_addrolemember N'db_datareader', N'applicationuser'
EXEC sp_addrolemember N'db_datawriter', N'applicationuser'
GO

CREATE FUNCTION dbo.TenantAccessPredicate (@TenantId uniqueidentifier)
    RETURNS TABLE
    WITH SCHEMABINDING
AS
    RETURN SELECT 1 AS TenantAccessPredicateResult
  WHERE (@TenantId = CAST(SESSION_CONTEXT(N'TenantId') AS uniqueidentifier))
      OR (DATABASE_PRINCIPAL_ID() = DATABASE_PRINCIPAL_ID('sa'))
GO

CREATE TABLE dbo.Tenant
(
  TenantId uniqueidentifier NOT NULL,
  TenantName nvarchar(200) NOT NULL,
  CONSTRAINT PK_Tenant PRIMARY KEY CLUSTERED (TenantId ASC)
)
GO

CREATE TABLE dbo.Person
(
  Id uniqueidentifier NOT NULL,
  TenantId uniqueidentifier NOT NULL,
  Name nvarchar(200) NOT NULL,
  CONSTRAINT PK_Application PRIMARY KEY CLUSTERED (Id ASC),
  CONSTRAINT FK_Application_Tenant FOREIGN KEY (TenantId) REFERENCES dbo.Tenant(TenantId)
)
GO

INSERT INTO dbo.Tenant(TenantId, TenantName)
VALUES('6CB8DE43-2043-4415-B267-7FFFA2EB5AC0','tenant 1')
INSERT INTO dbo.Tenant(TenantId, TenantName)
VALUES('25EA09EF-E24E-494B-911F-F63CE9ED8458','tenant 2')
GO

INSERT dbo.Person(Id, TenantId, Name)
VALUES ('E897FF55-8F3D-4154-B582-8D37D116347F', '6CB8DE43-2043-4415-B267-7FFFA2EB5AC0', N'Chai')
GO
INSERT dbo.Person(Id, TenantId, Name)
VALUES ('F5506F2A-5148-44FE-9225-AC75108AA30F', '6CB8DE43-2043-4415-B267-7FFFA2EB5AC0', N'Chang')
GO
INSERT dbo.Person(Id, TenantId, Name)
VALUES ('775EDB92-32BE-4D46-ABBB-921BC1860514', '6CB8DE43-2043-4415-B267-7FFFA2EB5AC0', N'Aniseed Syrup')
GO
INSERT dbo.Person(Id, TenantId, Name)
VALUES ('ED3D0EE0-3D02-460A-9659-58C34CCC663F', '25EA09EF-E24E-494B-911F-F63CE9ED8458', N'Chang')
GO
INSERT dbo.Person(Id, TenantId, Name)
VALUES ('CED038B1-DF1C-4111-9E48-E386A1A7063A', '25EA09EF-E24E-494B-911F-F63CE9ED8458', N'Pavlova')
GO

CREATE SECURITY POLICY dbo.TenantAccessPolicy
ADD FILTER PREDICATE dbo.TenantAccessPredicate(TenantId) ON dbo.Person,
ADD BLOCK PREDICATE dbo.TenantAccessPredicate(TenantId) ON dbo.Person
GO
